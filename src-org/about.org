#+title: About
#+EXPORT_FILE_NAME: about.html
#+options: toc:nil num:nil title:nil \n:t
#+PANDOC_OPTIONS: include-in-header:include/main.css wrap:none
#+pandoc_metadata: "pagetitle:About" "document-css:false"

#+include: include/positionlat.html export html
@@html:<div id="maindiv" style="text-align:center">@@
* About
@@html:<img src="assets/images/me.JPG" alt="autoportrait" height="500" class="center" style="margin:auto;display:block">@@
Hi, I'm Léo, I study music, I studied physics and I do other stuff :3

@@html:<p align=center><a href="https://akko.disqordia.space/leo_of_the_tisane"><img src="assets/images/disqordia.png" id=iconl style="float:lefti;"></a> <a href="https://brain.worm.pink/users/tisanae"><img src="assets/images/worm.png" id=iconl style="float:lefti;"></a> <a href="https://leoandthetisane.bandcamp.com/"><img src="assets/images/bandcamp.png" id=iconl style="float:lefti;"></a></p>@@

--------------

@@html:<p style="text-align:center"><a href="/index.html"><img src="/assets/images/mainfond.png" id=iconl></a></p>@@


@@html:</div>@@
#+include: include/footer.html export html
