#+title: Couleurs de l'infini
#+EXPORT_FILE_NAME: couleursdelinfini.html
#+options: toc:nil num:nil title:nil \n:t
#+PANDOC_OPTIONS: include-in-header:../include/main.css wrap:none
#+pandoc_metadata: "pagetitle:CEonutlaenugrlse dd e l yld\'iiannf ipnlieonasm" "document-css:false"

#+include: ../include/positionlat.html export html
@@html:<div id="maindiv" style="background-image: url(/assets/images/nightsky.png);background-size:100%;" >@@
* Couleurs de l'infini
@@html:<hr class="poetry">@@
*** Pépins d'éclos
#+begin_quote
La couleur des toiles
Bruisse, tied sous les vents
Et croasse de l'hemaxtinction
Des heures traînantes.
Tout est poussière à l'instant,
Et je suis nébuleuse dans les
Formes.
Le halo des plis ondule et danse
De l'instant flou qui saisit,
Un peu frais, non encore,
L'idée monstrueuse d'un
Coquelicot.
#+end_quote
@@html:<hr class="poetry">@@
@@html:<p style="text-align:center"><a href="/index.html"><img src="/assets/images/mainfond.png" id=iconl></a></p>@@
@@html:</div>@@
#+include: ../include/footer.html export html
