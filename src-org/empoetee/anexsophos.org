#+title: Les anes ex sophos
#+EXPORT_FILE_NAME: anexsophos.html
#+options: toc:nil num:nil title:nil \n:t
#+PANDOC_OPTIONS: include-in-header:../include/main.css wrap:none
#+pandoc_metadata: "pagetitle:LEenst aanngelse de xL ysdoipaons Pleonasm" "document-css:false"

#+include: ../include/positionlat.html export html
@@html:<div id="maindiv">@@
* Les anes ex sophos

@@html:<hr class="poetry">@@

#+begin_quote
L’autre nuit, j’ai fui
Une vie sans ailes,
Dévidant des flottilles
De jolis mots
Épars, s’étiolant
Comme tout ce qui
S'oublie.

#+end_quote

@@html:<hr class="poetry">@@

#+begin_quote
Des papillons se posent
Et aspirent mon âme
Tout comme le temps
Qu’émousse l’amer
Et la Mer qui ronge
Les ongles bordés de
L’onde. Un baiser
Serait une ondée
Et un chagrin d’Étoiles
Étiolées, sans regrets,
Brisés les Thésées de
Doutes, vive les soirées !

#+end_quote

@@html:<hr class="poetry">@@

#+begin_quote
Les mots sont des oiseaux,
Les maux des monts,
Mon âme s’y noie et
S’y nourrit, comme
Si j’y rêvais d’ailes

#+end_quote

@@html:<hr class="poetry">@@

@@html:<p style="text-align:center"><a href="/index.html"><img src="/assets/images/mainfond.png" id=iconl></a></p>@@
@@html:</div>@@
#+include: ../include/footer.html export html
