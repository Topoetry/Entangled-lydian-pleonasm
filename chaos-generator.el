;;; chaos-generator.el --- Generate my website with ox-pandoc -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Léo, chaos given form
;;
;; Author: Léo, chaos given form <leo.tisane@disroot.org>
;; Maintainer: Léo, chaos given form <leo.tisane@disroot.org>
;; Created: février 29, 2024
;; Modified: février 29, 2024
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://codeberg.org/Topoetry/Entangled-lydian-pleonasm
;; Package-Requires: ((emacs "24.4"))
;;
;; This file is not part of GNU Emacs.
;;
;;
;;; Commentary:
;; this should work, insh'allah
;;
;;
;;
;;; Code:
(unless (executable-find "pandoc")
  (print "Pandoc does not seems to be installed, exiting unconditionally")
  (setq quit-flag t))
(require 'package)
(setq package-user-dir (expand-file-name "./.packages"))
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

;; Initialize the package system
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))
(package-install 'ox-pandoc)
(require 'ox-publish)
(require 'ox-pandoc)
;; thanks stack overflow: makes ox-publish work with ox-pandoc
(defun chaos-generator-org-pandoc-html5-filter (contents _backend _info)
  "Convert Org CONTENTS into html5 output."
  (let ((backup-inhibited t)
        contents-filename
        process
        buffer)
    (unwind-protect
        ;; org-pandoc runs pandoc asynchronous.  We need to
        ;; synchronize pandoc for filtering.  `org-pandoc-run' returns
        ;; the process needed for synchronization.  Pityingly we need
        ;; to call `org-pandoc-run-to-buffer-or-file' which handles
        ;; additional options and special hooks.  Therefore we
        ;; temporarily advice `org-pandoc-run' to give us the process.
        (cl-letf* ((original-org-pandoc-run (symbol-function 'org-pandoc-run))
                   ((symbol-function 'org-pandoc-run) (lambda (&rest a)
                                                        (setq process (apply original-org-pandoc-run a)))))
          (setq contents-filename (make-temp-file ".tmp" nil ".org" contents))
          (org-pandoc-run-to-buffer-or-file
           contents-filename
           'html5
           nil ;; not only the sub-tree
           t) ;; buffer
          (while (process-live-p process)
            (sit-for 0.5))
          (with-current-buffer (setq buffer (process-buffer process))
            (buffer-string)))
      (when (file-exists-p contents-filename)
        (delete-file contents-filename))
      (when (buffer-live-p buffer)
        (kill-buffer buffer)))))

(org-export-define-derived-backend
 'pandoc-html5
 'pandoc
 :filters-alist '((:filter-final-output . chaos-generator-org-pandoc-html5-filter)))

(defun chaos-generator-org-pandoc-publish-to-html (plist filename pub-dir)
  "Publish an org file to html using ox-pandoc. Return output file name."
  (let ((org-pandoc-format "html5"))
    (org-publish-org-to
     'pandoc-html5
     filename
     (concat "." (or (plist-get plist :html-extension)
                     org-html-extension
                     "html"))
     plist
     pub-dir)));
;; Define the publishing project
(setq org-publish-project-alist
      (list
       (list "entangled-lydian-pleonasm"
             :recursive t
             :base-directory "./src-org"
             :publishing-directory "./public"
             :publishing-function 'chaos-generator-org-pandoc-publish-to-html)))

;; Generate the site output
(org-publish-all t)
(message
 "#######################################################################
#######################    Done :D     ################################
#######################################################################")
(provide 'chaos-generator)
;;; chaos-generator.el ends here
